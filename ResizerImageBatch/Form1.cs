using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;

using System.Text;
using System.Windows.Forms;

using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Threading.Tasks;
using System.Linq;


namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private Image Redimensionar(Image Imagen, int Ancho, int Alto, int resolucion)
        {
            //Bitmap sera donde trabajaremos los cambios
            using (Bitmap imagenBitmap = new Bitmap(Ancho, Alto, PixelFormat.Format32bppRgb))
            {
                imagenBitmap.SetResolution(resolucion, resolucion);
                //Hacemos los cambios a ImagenBitmap usando a ImagenGraphics y la Imagen Original(Imagen)
                //ImagenBitmap se comporta como un objeto de referenciado
                using (Graphics imagenGraphics = Graphics.FromImage(imagenBitmap))
                {
                    imagenGraphics.SmoothingMode = SmoothingMode.AntiAlias;
                    imagenGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    imagenGraphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    imagenGraphics.DrawImage(Imagen, new Rectangle(0, 0, Ancho, Alto), new Rectangle(0, 0, Imagen.Width, Imagen.Height), GraphicsUnit.Pixel);
                    //todos los cambios hechos en imagenBitmap lo llevaremos un Image(Imagen) con nuevos datos a travez de un MemoryStream
                    MemoryStream imagenMemoryStream = new MemoryStream();
                    imagenBitmap.Save(imagenMemoryStream, ImageFormat.Jpeg);
                    Imagen = Image.FromStream(imagenMemoryStream);
                }
            }

            return Imagen;
        }
        private Image Redimensionar(Image image, int SizeHorizontalPercent, int SizeVerticalPercent)
        {
            //Obntenemos el ancho y el alto a partir del porcentaje de tama�o solicitado
            int anchoDestino = image.Width * SizeHorizontalPercent / 100;
            int altoDestino = image.Height * SizeVerticalPercent / 100;
            //Obtenemos la resolucion original 
            int resolucion = Convert.ToInt32(image.HorizontalResolution);

            return this.Redimensionar(image, anchoDestino, altoDestino, resolucion);
        }

        private void Redimensionar(OpenFileDialog browseFile)
        {
            string contenedor = "";

            List<string> fileNameList = browseFile.FileNames.ToList();
            if (!fileNameList.Any())
                return;

            fileNameList = fileNameList.Where(x => x.ToUpper().Contains(".JPG")).ToList();

            FileInfo firstFileInfo = new FileInfo(fileNameList.First());
            contenedor = string.Format(@"{0}\ImagenesRedimensionadas\", firstFileInfo.Directory.FullName);
            if (!Directory.Exists(contenedor))
                Directory.CreateDirectory(contenedor);
            
            Parallel.ForEach(fileNameList, new ParallelOptions() { MaxDegreeOfParallelism = 10 }, file =>
            {
                var fileArgument = new FileArgument { Contenedor = contenedor, FullName = file };

                this.GenerarImagen(contenedor, file);
            });
        }

        //Backgroundworker
        //private void GenerarImagen(object sender, DoWorkEventArgs e)
        //{ FileArgument fileArgument = (FileArgument)e.Argument; }
        private void GenerarImagen(string contenedor, string fullNameFile)
        {
            Image image = null;
            //Obtenemos la imagen del filesystem
            using (image = Image.FromFile(fullNameFile))
            {
                //Redimensionamos la imagen
                image = this.Redimensionar(image, Convert.ToInt32(textBox1.Text), Convert.ToInt32(textBox2.Text));

                //Guardamos la imagen modificada 
                image.Save(string.Format(@"{0}{1}", contenedor, fullNameFile.Split('\\').ToList().Last()));
            }
        }



        private void RedimensionarOLD(OpenFileDialog browseFile)
        {
            string contenedor = "";
            string fileName;
            string[] Array;

            foreach (string file in browseFile.FileNames)
            {
                if (file.Substring(file.Length - 4, 4).ToUpper() == ".JPG")
                {
                    var task = new Task(() =>
                    {
                        Image image = null;
                        //Obtenemos la imagen del filesystem
                        using (image = Image.FromFile(@file))
                        {
                            //Redimensionamos la imagen
                            image = this.Redimensionar(image, Convert.ToInt32(textBox1.Text), Convert.ToInt32(textBox2.Text));

                            Array = file.Split('\\');
                            contenedor = file.Substring(0, file.LastIndexOf("\\") + 1);
                            contenedor += @"\ImagenesRedimensionadas\";
                            if (!Directory.Exists(contenedor))
                                Directory.CreateDirectory(contenedor);

                            fileName = Array[Array.Length - 1];

                            //Guardamos la imagen modificada 
                            image.Save(string.Format(@"{0}{1}", contenedor, fileName));
                        }
                    });
                    task.Start();

                }
            }

            //System.Diagnostics.Process.Start(@contenedor);
            //button2.Enabled = true;
        }
        private void button2_Click(object sender, EventArgs e)
        {


            try
            {
                OpenFileDialog browseFile = new OpenFileDialog();
                browseFile.Filter = "JPG Files (*.jpg)|*.jpg";
                browseFile.Multiselect = true;
                if (browseFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    var task = new Task(() => this.Redimensionar(browseFile));
                    task.Start();
                }
                //button2.Enabled = false;

            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }

        private void ClearMemory()
        {
            GC.Collect(2, GCCollectionMode.Optimized);
        }
        private void button3_Click(object sender, EventArgs e)
        {
            this.ClearMemory();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
                textBox2.Text = textBox1.Text;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
                textBox1.Text = textBox2.Text;
        }
        #region Recorte de fotos DNI
        public void AgregarAlLog(string FileFullPath, string valor)
        {
            using (StreamWriter sw = File.AppendText(FileFullPath))
            {
                sw.WriteLine(string.Format("Archivo numero {0} a las {1}", valor, DateTime.Now.ToString()));
            }
        }

        private Image RecortarImagen(ref Bitmap imageDimensionada, ref Image image, int x0, int y0, ref Rectangle recorte/*int x1, int y1, int x2, int y2*/)
        {
            //tama�o deseado
            //Bitmap image2 = new Bitmap(82, 108);
            using (Graphics lienzo = Graphics.FromImage(imageDimensionada))
            {
                //Rectangle recorte = new Rectangle(x1, y1, x2, y2);
                lienzo.DrawImage(image, x0, y0, recorte, GraphicsUnit.Pixel);
            }
            return imageDimensionada;
        }

        #endregion
    }

    public class FileArgument
    {
        public string Contenedor { get; set; }
        public string FullName { get; set; }
    }
}